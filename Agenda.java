import java.util.*;

public class Agenda{
private ArrayList<Contatos> listaContatos;
	public static void main (String args[]){
		//Declaração de atributos, leitura do teclado e ArrayList.
		Scanner lerTeclado = new Scanner(System.in);		
		ArrayList<Contatos> listaContatos = new ArrayList<Contatos>();
		String Nome;
		String Telefone;
		int Sexo, i, opcao, cont_remover;	
		do{	
			//Laço do menu.
			System.out.println("------------------------------------------------------------------------");
			System.out.println("Menu: \n");			
			System.out.println("1-Criar novo contato");
			System.out.println("2-Listar contatos");
			System.out.println("3-Excluir contatos\n");
			System.out.println("0-Sair\n\n");
			System.out.println("Qual a sua opção?");
			opcao=lerTeclado.nextInt();
			System.out.println("---------------------------------");
			switch(opcao){
					case 1:	
					//Caso para a criação de um contato.
					for(i=0; i< 24; i++) //Estrutura para limpar a tela('clear' do terminal). 
						System.out.println("\n"); 	
					
					lerTeclado.nextLine();			
					System.out.println("Insira o nome do contato: ");
					Nome=lerTeclado.nextLine();		
					System.out.println("Insira o telefone do contato: ");
					Telefone=lerTeclado.next();
					System.out.println("Insira o sexo do contato(masculino=1 e feminino=0): ");
					lerTeclado.nextLine();
					Sexo=lerTeclado.nextInt();
										
					Contatos umContato = new Contatos(Nome, Telefone);
					umContato.setSexo(Sexo);
					listaContatos.add(umContato);
					System.out.println("\n\nContato adicionado!");
					break;			
				case 2: 
					//Caso para a listagem dos contatos.
					for(i=0; i< 24; i++)
						System.out.println("\n");
					
					System.out.println("Quantidade de contatos: " +listaContatos.size());
					
					for(i=0;i<listaContatos.size();i++){
						System.out.println("\nContato nº: "+(1+i));
						Contatos contato= listaContatos.get(i);
						System.out.println("Nome:" +contato.getNome());
						System.out.println("Telefone:" +contato.getTelefone());
						System.out.println("Sexo:" +contato.getSexo()+"\n");
					}
					break;
				case 3:	
					//Caso para remoção de contatos.
					System.out.println("Qual o nº do contato que deseja remover?");
					cont_remover=lerTeclado.nextInt();
					listaContatos.remove(cont_remover-1);
					System.out.println("\n\nContato removido!");
					break;
				case 0:
					System.out.println("\n\nSaindo...");
					break;
				default:		
					System.out.println("Opção incorreta.");

				}
			}while(opcao!=0);//Gatilho para o fim do laço.
			
		
	}
}
