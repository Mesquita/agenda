public class Contatos{
	//Declaração dos atributos.
	private String nome;
	private String telefone;
	private int sexo; //1 para masculino e 0 para feminino.
	
	//Mŕtodo construtor de Contatos.
	public Contatos(String nome, String telefone){
		this.nome=nome;
		this.telefone=telefone; 
	}
	//Métodos acessadores do atributo (getters e setters).
	public String getNome(){
		return nome;
	}
	
	public String getTelefone(){
		return telefone;
	}

	public void setSexo(int umSexo){
		sexo=umSexo;
	}

	public String getSexo(){
		String sex;
		if(sexo==1){
			sex="Masculino";
		} 
		else{
			sex="Feminino";
		}		
		return sex;
		}
}

